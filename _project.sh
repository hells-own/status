_build(){
    make
}

_rebuild(){
    make clean status
}

_launch(){
    _build
    ./status
}

_debug(){
    echo -n ./status -s status -ex break
}

_rebuild-tags(){
    gtags
}
