#ifndef _STATUS_H
#define _STATUS_H

#define _POSIX_C_SOURCE 2

#include <time.h>

struct buffer {
    unsigned int len;
    char dat[1];
};
typedef struct buffer *segment;

union arg {
    int si;
    unsigned int ui;
    void *ptr;
    const char *str;
};

struct component {
    segment buf; 
    const char *fmt;
    union arg arg;
    void *data;
    void (*destroy) (void *ptr);
};

typedef int (*timer_accept) (struct component *c, time_t now);
typedef time_t (*timer_next) (struct component *c, time_t now);
typedef int (*notify_accept) (struct component *c, const char *path, int fd);
typedef int (*listen_accept) (struct component *c, const char *msg, const char *rest);

struct timer {
    time_t again;
    timer_next next;
    timer_accept accept;
    struct component *c;
};

struct notify {
    const char *path;
    int fd;
    int watch;
    notify_accept accept;
    struct component *c;
};

struct listen {
    const char *kwd;
    listen_accept accept;
    struct component *c;
};

int *timer_add(timer_next next, timer_accept accept, struct component *c);
struct timer *timer_add_controlled(timer_accept accept);
int notify_add(const char *path, notify_accept accept, struct component *c);
int listen_add(const char *kwd, listen_accept accept, struct component *c);
struct buffer *segment_add(unsigned short buffer_length);

void component_setup();

#endif
