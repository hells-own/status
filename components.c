#include "status.h"
#include "components.h"

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

void wfclose(void * ptr)
{
    fclose(ptr);
}

int timer_accept_print_localtime(struct component *c, time_t now)
{
    struct tm *t;
    t = localtime(&now);
    strftime(c->buf->dat, c->buf->len, c->fmt, t);
    return 1;
}

time_t timer_next_period(struct component *c, time_t now)
{
    return now + c->arg.ui;
}

time_t timer_next_align_day(struct component *c, time_t now)
{
    struct tm *t;
    int daytime, i;
    t = localtime(&now);
    daytime = t->tm_sec + 60 * (t->tm_min + 60 * t->tm_hour);
    i = daytime % c->arg.ui;
    return now + c->arg.ui - i;
}

int listen_accept_echo(struct component *c, const char *msg, const char *rest)
{
    if (rest == 0 || strncmp(c->buf->dat, rest, c->buf->len) == 0)
        return 0;
    snprintf(c->buf->dat, c->buf->len, c->fmt, rest);
    return 1;
}

int notify_accept_echo(struct component *c, const char *path, int fd)
{
    char text[256];
    int i;
    i = read(fd, text, sizeof text);
    if (i < 1 || strncmp(c->buf->dat, text, c->buf->len) == 0)
        return 0;
    snprintf(c->buf->dat, c->buf->len, c->fmt, text);
    return 1;
}

void localtime_setup(unsigned int interval, const char *format)
{
    struct component *c;
    c = malloc(sizeof(struct component));
    c->buf = segment_add(64);
    c->arg.ui = interval;
    c->fmt = format;
    c->destroy = free;
    timer_add(timer_next_align_day, timer_accept_print_localtime, c);
}

int timer_accept_read(struct component *c, time_t now)
{
    int fd;
    char buf[128], *e;
    if (!c->data)
    {
        fprintf(stderr, "ERR timer fd \n");
        return 0;
    }
    rewind(c->data);
    if (fgets(buf, sizeof buf, c->data) != buf)
    {
        fprintf(stderr, "ERR timer read \n");
        return 0;
    }
    if(e = strchr(buf, '\n'))
        *e = 0;
    fprintf(stderr, "read: %s\n", buf);
    snprintf(c->buf->dat, c->buf->len, c->fmt, buf);
    return 1;
}

int timer_accept_read_path(struct component *c, time_t now)
{
    FILE *file;
    char buf[128], *e;
    file = fopen(c->data, "r");
    if (!file)
    {
        fprintf(stderr, "ERR open path %s\n", (const char*)c->data);
        return 0;
    }
    if (fgets(buf, sizeof buf, file) != buf)
    {
        fprintf(stderr, "ERR read path %s\n", (const char*)c->data);
        return 0;
    }
    if ((e = strchr(buf, '\n')))
        *e = 0;
    fprintf(stderr, "read: %s\n", buf);
    snprintf(c->buf->dat, c->buf->len, c->fmt, buf);
    return 1;
}

void timer_read_setup(unsigned int period, const char *path, const char *format)
{
    struct component *c;
    c = malloc(sizeof(struct component));
    c->buf = segment_add(128);
    c->data = fopen(path, "r");
    c->destroy = wfclose;
    c->fmt = format;
    c->arg.ui = period;
    timer_add(timer_next_align_day, timer_accept_read, c);
}

void timer_read_path_setup(unsigned int period, const char *path, const char *format)
{
    struct component *c;
    c = malloc(sizeof(struct component));
    c->buf = segment_add(128);
    c->data = strdup(path);
    c->destroy = free;
    c->fmt = format;
    c->arg.ui = period;
    timer_add(timer_next_align_day, timer_accept_read_path, c);
}

void battery_capacity_setup(unsigned int period, const char *dev, const char *format)
{
    char path[256];
    snprintf(path, sizeof path, "/sys/bus/acpi/drivers/battery/%s/capacity", dev);
    fprintf(stderr, "battery path=%s\n", path);
    timer_read_path_setup(period, path, format);
}

void battery_status_setup(unsigned int period, const char *dev, const char *format)
{
    char path[256];
    snprintf(path, sizeof path, "/sys/bus/acpi/drivers/battery/%s/status", dev);
    fprintf(stderr, "battery path=%s\n", path);
    timer_read_path_setup(period, path, format);
}
