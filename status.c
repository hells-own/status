#include "status.h"

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>

struct _cons {
    void *car;
    struct _cons *cdr;
};

typedef struct _cons cons;

static void *push(size_t s, cons **l);
static cons *pop(void *p, size_t s, cons **l); 

static int notify_setup();
static int notify_read();
static int msg_setup();
static int msg_read();
static int poll_setup();
static int poll_once();
static int reprint();
static void destroy();

static void xroot_setup();
static void xroot_destroy();
static void xroot_set(const char *name, size_t len);

const char *msg_socket_format = "/tmp/status-%d.sock";

static cons _nil = { NULL, NULL };
static cons *const NIL = &_nil;
static cons *timer_list = NIL;
static cons *notify_list = NIL;
static cons *listen_list = NIL;
static cons *segment_list = NIL;

static int inotify_fd = -1;
static int msg_sock = -1;
static int epoll_fd = -1;
static struct epoll_event epoll_events[1];

static char msg_buf[1024];
static int run = 1;
static int restart = 0;

static xcb_connection_t *xcb_dpy;
static xcb_screen_t *xcb_screen;
static xcb_window_t xcb_root;

#define EPOLL_EVENTS_N (sizeof(epoll_events) / sizeof(struct epoll_event))
#define SIZEOF_CONS sizeof(struct _cons)
#define SIZEOF_PTR sizeof(void*)
#define SERR(MSG) do { fprintf(stderr, __FILE__ ":%d " MSG ": %d %s\n", __LINE__ , errno, strerror(errno) ); exit(1); } while (0);

void *push(size_t s, cons **l)
{
    void *v;
    cons *c;
    c = malloc(SIZEOF_CONS);
    v = malloc(s);
    c->car = v;
    c->cdr = *l;
    *l = c;
    return v;
}

cons *pop(void *p, size_t s, cons **l)
{
    cons *c;
    if (p)
        memcpy(p, (*l)->car, s);
    free((*l)->car);
    c = (*l)->cdr;
    free(*l);
    *l = c;
    return c;
}

int *timer_add(timer_next next, timer_accept accept, struct component *c)
{
    struct timer *t;
    t = push(sizeof(*t), &timer_list);
    t->again = 0;
    t->next = next;
    t->accept = accept;
    t->c = c;
    fprintf(stderr, "timer_add %p\n", t);
    return 0;
}

struct timer *timer_add_controlled(timer_accept accept)
{
    struct timer *t;
    t = push(sizeof(*t), &timer_list);
    t->again = 0;
    t->next = NULL;
    t->accept = accept;
    t->c = NULL;
    fprintf(stderr, "timer_add_controlled %p\n", t);
    return t;
}

int notify_add(const char *path, notify_accept accept, struct component *c)
{
    struct notify *n;
    n = push(sizeof(*n), &notify_list);
    n->path = path;
    n->fd = -1;
    n->watch = -1;
    n->accept = accept;
    n->c = c;
    return 0;
}

int listen_add(const char *kwd, listen_accept accept, struct component *c)
{
    struct listen *l;
    l = push(sizeof(*l), &listen_list);
    l->kwd = kwd;
    l->accept = accept;
    l->c = c;
    return 0;
}

struct buffer *segment_add(unsigned short buffer_length)
{
    struct buffer *ptr;
    ptr = push(buffer_length + sizeof(struct buffer), &segment_list);
    ptr->len = buffer_length;
    ptr->dat[0] = '\0';
    return ptr;
}

int notify_setup()
{
    cons *c;
    struct notify *n;

    if ((inotify_fd = inotify_init()) == -1)
        SERR("inotify_init");

    for (c = notify_list; c != NIL; c = c->cdr)
    {
        n = (struct notify*) c->car;
        if ((n->fd = open(n->path, O_RDONLY)) == -1)
            SERR("notify_setup open");
        if ((n->watch = inotify_add_watch(inotify_fd, n->path, IN_MODIFY)) == -1)
            SERR("notify_setup inotify_add_watch");
    }

    return 0;
}

int notify_read()
{
    struct inotify_event event;
    struct notify *n;
    cons *c;
    int i = 0, ret;

    while ((ret = read(inotify_fd, &event, sizeof(event))) == sizeof(event) && ++i)
    {
        if (event.mask == IN_MODIFY)
            for (c = notify_list; c != NIL; c = c->cdr, n = (struct notify*) c->car)
                n->accept(n->c, n->path, n->fd);
        else
            fprintf(stderr, "notify_read unexpected event: %x\n", event.mask);
        ++i;
    }

    if (ret == -1)
        SERR("notify_read");

    return i;
}

int msg_setup()
{
    int i;
    struct sockaddr_un addr;

    if(-1 == (msg_sock = socket(AF_UNIX, SOCK_DGRAM, 0)))
       SERR("socket");

    memset(&addr, 0, sizeof addr);
    addr.sun_family = AF_UNIX;
    snprintf(addr.sun_path, sizeof(addr.sun_path) - 1, msg_socket_format, getuid());

    unlink(addr.sun_path);

    if (-1 == bind(msg_sock, (const struct sockaddr *) &addr, sizeof addr))
        SERR("bind");

    if (!1 == listen(msg_sock, 20))
        SERR("listen");
    
    return 0;
}

int msg_read()
{
    char *p;
    int i, update;
    cons *c;
    struct listen *m;
    update = 0;
    if (i = recv(msg_sock, msg_buf, sizeof msg_buf, 0))
    {
        msg_buf[i] = 0;

        if (p = strchr(msg_buf, ' '))
            *p++ = '\0';

        if (strcmp(msg_buf, "quit") == 0)
        {
            run = 0;
            return 0;
        }
        else if (strcmp(msg_buf, "restart") == 0)
        {
            run = 0;
            restart = 1;
            return 0;
        }
        for (c = listen_list, m = c->car; c != NIL; c = c->cdr, m = c->car)
            if (strcmp(msg_buf, m->kwd) == 0)
                update |= m->accept(m->c, msg_buf, p);
        fprintf(stderr, "%s: %s\n", msg_buf, p);
    }
    return update;
}

int poll_setup()
{
    struct epoll_event ev;
    ev.events = EPOLLIN | EPOLLET;

    if ((epoll_fd = epoll_create1(0)) == -1)
        SERR("poll_setup epoll_create");

    ev.data.fd = inotify_fd;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &ev) == -1)
        SERR("poll_setup epoll_ctl inotify_fd");

    ev.data.fd = msg_sock;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, msg_sock, &ev) == -1)
        SERR("poll_setup epoll_ctl msg_sock");
    
    return 0;
}

int poll_once(int timeout)
{
    int n, update, i;

    if ((n = epoll_wait(epoll_fd, epoll_events, EPOLL_EVENTS_N, timeout)) == -1)
        fprintf(stderr, "poll_once epoll_wait errno=%d %s\n", errno, strerror(errno));

    update = 0;
    for (i = 0; i < n; i++)
        if (epoll_events[i].events & EPOLLIN)
        {
            if (epoll_events[i].data.fd == inotify_fd)
                update |= notify_read();
            if (epoll_events[i].data.fd == msg_sock)
                update |= msg_read();
        }

    return update;
}

int reprint()
{
    cons *c;
    struct buffer *buf;
    char name[512];
    size_t len = 0;
    for (c = segment_list, buf = c->car; c != NIL; c = c->cdr, buf = c->car)
    {
        fputs(buf->dat, stdout);
        len += snprintf(name + len, sizeof name - len, "%s", buf->dat);
    }
    fputs("\n", stdout);
    xroot_set(name, len);
}

int loop()
{
    int wait, update;
    time_t now;
    cons *c;
    struct timer *tim;

    wait = 10 * 60;
    time(&now);
    for (c = timer_list, tim = c->car; c != NIL; c = c->cdr, tim = c->car)
        if (tim->again != -1)
        {
            if (tim->again <= now)
                wait = 0;
            else if (tim->again - now < wait)
                wait = tim->again - now;
        }

    update = poll_once(wait * 1000);

    time(&now);
    for (c = timer_list, tim = c->car; c != NIL; c = c->cdr, tim = c->car)
        if (tim->again <= now)
        {
            fprintf(stderr, "timer exec %p\n", tim);
            update |= tim->accept(tim->c, now);
            if (tim->next)
                tim->again = tim->next(tim->c, now);
        }

    if (update)
        reprint();

    return run;
}

void destroy()
{
    struct timer t;
    struct notify n;
    struct listen l;

    if (inotify_fd != -1)
    {
        close(inotify_fd);
        inotify_fd = -1;
    }

    while (notify_list != NIL)
    {
        pop(&n, sizeof(n), &notify_list);
        if (n.fd != -1)
        {
            close(n.fd);
            n.fd = -1;
        }
        if (n.c && n.c->destroy)
            n.c->destroy(n.c);
    }

    if (msg_sock != -1)
    {
        close(msg_sock);
    }

    if (epoll_fd != -1)
    {
        close(epoll_fd);
        epoll_fd = -1;
    }

    while (listen_list != NIL)
    {
        pop(&l, sizeof(struct listen), &listen_list);
        if (l.c && l.c->destroy)
            l.c->destroy(l.c);
    }
    while (timer_list != NIL)
    {
        pop(&t, sizeof(struct timer), &timer_list);
        if (t.c && t.c->destroy)
            t.c->destroy(t.c);
    }
    while (segment_list != NIL)
        pop(NULL,
            ((struct buffer*)segment_list->car)->len
            + sizeof(struct buffer),
            &segment_list);
}

int test()
{
    int i;
    cons *c = NIL;
    for (i = 0; i < 10; i++)
        printf("%i: %p\n", i, *(int*)push(sizeof(i), &c) = i);
    printf("\n");
    while (c != NIL)
        printf("%i: %p\n", (pop(NULL, sizeof(i), &c), i), c);
}

void xroot_setup()
{
    int screen_nbr;
    xcb_dpy = xcb_connect(NULL, &screen_nbr);
    if (xcb_connection_has_error(xcb_dpy))
        SERR("xroot_setup xcb_connect\n");
    xcb_screen = xcb_aux_get_screen(xcb_dpy, screen_nbr);
    xcb_root = xcb_screen->root;
}

void xroot_set(const char *name, size_t len)
{
    xcb_change_property(xcb_dpy, XCB_PROP_MODE_REPLACE, xcb_root,
                        XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, len, name);
    xcb_flush(xcb_dpy);
}

void xroot_destroy()
{
    xcb_disconnect(xcb_dpy);
}

int msg_concat(int argc, char *const *argv)
{
    int i, len, tlen;
    for (i = 2, tlen = 0; i < argc; i++)
    {
        len = strlen(argv[i]);
        if (len + tlen >= sizeof(msg_buf) - 1)
            break;
        if (tlen)
            msg_buf[tlen++] = ' ';
        strcpy(msg_buf + tlen, argv[i]);
        tlen += len;
    }
    return tlen;
}

int msg_send(int argc, char *const *argv)
{
    int len;
    struct sockaddr_un addr;

    len = msg_concat(argc, argv);
    if (!len)
        SERR("msg expected");

    if(-1 == (msg_sock = socket(AF_UNIX, SOCK_DGRAM, 0)))
       SERR("socket");

    memset(&addr, 0, sizeof addr);
    addr.sun_family = AF_UNIX;
    snprintf(addr.sun_path, sizeof(addr.sun_path) - 1, msg_socket_format, getuid());

    if (-1 == connect(msg_sock, (const struct sockaddr *) &addr, sizeof addr))
        SERR("connect");

    send(msg_sock, msg_buf, len, 0);
    close(msg_sock);

}

int main(int argc, char *const *argv)
{
    if (argc > 1 && !strcmp(argv[1], "-m"))
    {
        return msg_send(argc, argv);
    }
    
    xroot_setup();
    components_setup();

    notify_setup();
    msg_setup();
    poll_setup();

    while(loop());

    destroy();
    xroot_destroy();

    if (restart)
        execvp(argv[0], argv);
}

