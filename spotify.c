#include "status.h"
#include "components.h"

#include <string.h>
#include <stdio.h>

const char *SPOTIFY_FORMAT _WEAK =
    "[\\(.item.name)]"
    "[\\(.item.album.name) \\(.item.track_number)/\\(.item.album.total_tracks)]"
    "[\\(.item.artists[0].name)]";
int SPOTIFY_BAR_WIDTH _WEAK = 10;

#define SPOTIFY_AGAIN_FETCH(T) \
    spotify_timer->again = T, spotify_do_fetch = 1, spotify_fetch_count ++

#define SPOTIFY_AGAIN_PRINT(T) \
    spotify_timer->again = T, spotify_do_fetch = 0, spotify_fetch_count = 0

#define SPOTIFY_ERROR(WHAT) do {                                    \
        if (WHAT) {                                                 \
            snprintf(spotify->dat, spotify->len, "[err %s]", WHAT); \
            SPOTIFY_AGAIN_FETCH(now + 5 * 60);                      \
        } else {                                                    \
            sprintf(spotify->dat, "");                              \
            SPOTIFY_AGAIN_FETCH(now + 2 * 60);                      \
        }                                                           \
        return;                                                     \
    } while (0);

#define SPOTIFY_FETCH_MAX 10

static segment       spotify;
static char          spotify_buffer[128];
static struct timer *spotify_timer;
static int           spotify_do_fetch = 1;
static time_t        spotify_duration;
static time_t        spotify_start;
static int           spotify_fetch_count;

void spotify_setup()
{
    spotify = segment_add(128);
    fprintf(stderr, "barwidth=%d\n", SPOTIFY_BAR_WIDTH);
    listen_add("spotify-refresh", spotify_listen_accept, NULL);
    spotify_timer = timer_add_controlled(spotify_timer_accept);
}

void spotify_print(time_t now)
{
    time_t bar_div;
    int show_bar, bar_progress, i;
    char *wptr;

    if (SPOTIFY_BAR_WIDTH > 0)
    {
        bar_div = spotify_duration / SPOTIFY_BAR_WIDTH;

        if (now < spotify_start)
            bar_progress = 0;
        else if (now >= spotify_start + spotify_duration)
            bar_progress = SPOTIFY_BAR_WIDTH;
        else
            bar_progress = (now - spotify_start) / bar_div;

        if (spotify->len < SPOTIFY_BAR_WIDTH + 2)
            SPOTIFY_ERROR("len");

        spotify->dat[0] = '[';
        for (i = 0; i < SPOTIFY_BAR_WIDTH; i++)
            spotify->dat[i+1] = i > bar_progress ? ' '
                : i == bar_progress ? '>' : '=';
        i++, spotify->dat[i++] = ']';
    }
    else
        i = 0;

    strncpy(spotify->dat + i, spotify_buffer, spotify->len - i);

    if (SPOTIFY_BAR_WIDTH > 0 && bar_progress < SPOTIFY_BAR_WIDTH)
        SPOTIFY_AGAIN_PRINT(spotify_start + bar_div + bar_div * bar_progress);
    else if (spotify_fetch_count < SPOTIFY_FETCH_MAX)
        SPOTIFY_AGAIN_FETCH(spotify_start + spotify_duration);
    else
        SPOTIFY_ERROR("");
}

void spotify_fetch(time_t now)
{
    FILE *fp;
    char *c, *e, *err;
    char cmd[256];
    size_t len;

    fputs("spotify_fetch\n", stderr);
    spotify_buffer[0] = '\0';
    spotify->dat[0] = '\0';
    snprintf(cmd, sizeof cmd,
             "spotify-ctl"
             " -r"
             " '\\(.is_playing)  \\(.item.duration_ms) \\(.progress_ms) %s'",
             SPOTIFY_FORMAT);
    fp = popen(cmd, "r");
    if (fp == NULL)
        SPOTIFY_ERROR("popen");

    c = fgets(spotify_buffer, sizeof(spotify_buffer), fp);
    pclose(fp);
    if (!c)
        SPOTIFY_ERROR(NULL);

    if (!(c = strchr(spotify_buffer, ' ')))
        SPOTIFY_ERROR("format");
    *c++ = 0;

    if (strcmp(spotify_buffer, "true"))
        SPOTIFY_ERROR(NULL);

    spotify_duration = strtol(c, &e, 10) / 1000;
    if (spotify_duration < 1)
        SPOTIFY_ERROR(NULL);
    if (e == c)
        SPOTIFY_ERROR("format");

    c = e;
    spotify_start = now - strtol(c, &e, 10) / 1000;

    len = strlen(++e);
    memmove(spotify_buffer, e, len);
    spotify_buffer[len] = '\0';

    spotify_print(now);
    return;
}

int spotify_listen_accept(struct component *c, const char *msg, const char *rest)
{
    spotify_fetch(time(NULL));
    return 1;
}

int spotify_timer_accept(struct component *c, time_t now)
{
    if (spotify_do_fetch)
        spotify_fetch(now);
    else
        spotify_print(now);
    if (spotify_timer->again < now + 2)
        spotify_timer->again = now + 2;
    return 1;
}
