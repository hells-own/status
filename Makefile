DBGFL=-Og -gdwarf 
CFLAGS=-D_XOPEN_SOURCE=500 -ansi -Werror -Wpedantic $(DBGFL)
LDLIBS=-lxcb -lxcb-util

all: status

config.c:
	cp $@.ref $@

status: status.o config.o components.o spotify.o

clean:
	rm -f *.o status

.PHONY: clean
