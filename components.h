#ifndef _COMPONENTS_H
#define _COMPONENTS_H

#include "status.h"

#define _WEAK __attribute__((weak))

/* timer */
int timer_accept_print_localtime(struct component *c, time_t now);
time_t timer_next_period(struct component *c, time_t now);
time_t timer_next_align_day(struct component *c, time_t now);
void timer_read_setup(unsigned int interval, const char *path, const char *format);

/* notify */
int notify_accept_echo(struct component *c, const char *path, int fd);
void wfclose(void * ptr);

/* listen */
int listen_accept_echo(struct component *c, const char *msg, const char *rest);

/* date */
void date_setup(unsigned int interval, const char *format);

/* battery */
void battery_capacity_setup(unsigned int period, const char *dev, const char *format);
void battery_status_setup(unsigned int period, const char *dev, const char *format);

/* spotify */
extern const char *SPOTIFY_FORMAT;
extern int SPOTIFY_BAR_WIDTH;

void spotify_setup();
void spotify_fetch(time_t now);
void spotify_print(time_t now);
int spotify_listen_accept(struct component *c, const char *msg, const char *rest);
int spotify_timer_accept(struct component *c, time_t now);

#endif
